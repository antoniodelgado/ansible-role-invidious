## ansible-role-invidious

Role to deploy an instance of Invidious https://invidious.io/ using docker composer.
Check their documentation for more information about post-install tasks and caveats. https://docs.invidious.io/installation/#docker

# Requirements

https://gitlab.com/antoniodelgado/ansible-role-apache_ssl_vhosts


# Installation

Add it to your requirements.txt with the apache_ssl_vhosts (if you want it to handle Apache2)

```yaml
roles:
  - name: invidious
    src: ssh://git@repos.susurrando.com:1122/srv/git.repos/ansible-role-invidious.git
    scm: git
  - name: apache_ssl_vhosts
    src: ssh://git@repos.susurrando.com:1122/srv/git.repos/ansible-role-apache_ssl_vhosts
    scm: git
```

# Configuration

Check defaults/main.yml for some example configuration and the defaults.

# Usage

Just apply the role.

# Credits

Antonio J. Delgado 2022 https://gitlab.com/antoniodelgado
